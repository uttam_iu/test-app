import React, { Component } from 'react';
import { View } from 'react-native';
import MainView from './components/MainView';

class App extends Component {
	render() {
		return (<View>
			<MainView />
		</View>);
	}
}

export default App;