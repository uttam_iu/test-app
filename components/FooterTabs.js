import React, { Component } from 'react';
import { Footer, FooterTab, Button, Icon, Title, Text, Badge } from 'native-base';

export default class FooterTabs extends Component {
	render() {
		return (
			<Footer>
				<FooterTab>
					<Button active badge vertical>
						<Badge ><Text>51</Text></Badge>
						<Icon active name="navigate" />
						<Text>Navigate</Text>
					</Button>
					<Button vertical>
						<Icon name="person" />
						<Text>Contact</Text>
					</Button>
				</FooterTab>
			</Footer>
		);
	}
}