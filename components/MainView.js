import React, { Component } from 'react';
import { Container, Header, Content, Footer, FooterTab, Left, Body, Right, Button, Icon, Title, Text, Badge } from 'native-base';
import { View } from 'react-native';
import FooterTabs from './FooterTabs';
import HeaderPart from './HeaderPart';

export default class MainView extends Component {
	render() {
		return (
			<Container>
				<HeaderPart />
				<View style= {{marginTop: 100, height: 100, width: 200, backgroundColor: "red"}}/>
				<FooterTabs />
			</Container>
		);
	}
}